package com.epam.rd.java.basic.task8;

public class VisualParametrs{
	private String stemColour;
	private String leafColour;
	private String aveLenFlower;
	
	
	public VisualParametrs(String stemColour, String leafColour, String aveLenFlower) {
		super();
		this.stemColour = stemColour;
		this.leafColour = leafColour;
		this.aveLenFlower = aveLenFlower;
	}
	
	public VisualParametrs() {
		
	}


	
	public String getStemColour() {
		return stemColour;
	}

	public void setStemColour(String stemColour) {
		this.stemColour = stemColour;
	}

	public String getLeafColour() {
		return leafColour;
	}

	public void setLeafColour(String leafColour) {
		this.leafColour = leafColour;
	}

	public String getAveLenFlower() {
		return aveLenFlower;
	}

	public void setAveLenFlower(String aveLenFlower) {
		this.aveLenFlower = aveLenFlower;
	}


	@Override
	public String toString() {
		return "VisualParametrs:\n\t" + "stemColour: " + stemColour + "\n\tleafColour: "
				+ leafColour + "\n\taveLenFlower: " + aveLenFlower;
	}


	
	
}
