package com.epam.rd.java.basic.task8.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Util;
import com.epam.rd.java.basic.task8.VisualParametrs;
import com.epam.rd.java.basic.task8.constant.XMLconstants;
import com.epam.rd.java.basic.task8.constant.XMLflowerConstant;
/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Document xmlDocument;
	private List<Flower> flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	public void parseXML(boolean validate) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		if (validate) {
			try {
				dbf.setFeature(XMLconstants.FEATURE_TURN_VALIDATION_ON, true);
				dbf.setFeature(XMLconstants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
			} catch (ParserConfigurationException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}

		try {
			xmlDocument = dbf.newDocumentBuilder().parse(xmlFileName);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList flowerList = xmlDocument.getElementsByTagName(XMLflowerConstant.FLOWER);
		for (int j = 0; j < flowerList.getLength(); j++) {

			Flower flower = new Flower();
			NodeList elementList = flowerList.item(j).getChildNodes();
			for (int i = 0; i < elementList.getLength(); i++) {

				if (elementList.item(i).getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				switch (elementList.item(i).getNodeName()) {
					case XMLflowerConstant.FLOWER_NAME:
						flower.setName(elementList.item(i).getTextContent());
						break;
					case XMLflowerConstant.FLOWER_SOIL:
						flower.setSoil(elementList.item(i).getTextContent());
						break;
					case XMLflowerConstant.FLOWER_ORIGIN:
						flower.setOrigin(elementList.item(i).getTextContent());
						break;
					case XMLflowerConstant.VISUAL_PARAMETRS:
						NodeList visualElement = elementList.item(i).getChildNodes();
						VisualParametrs visualParametrs = new VisualParametrs();
						for (int v = 0; v < visualElement.getLength(); v++) {
							if (visualElement.item(v).getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}
							switch (visualElement.item(v).getNodeName()) {
								case XMLflowerConstant.VP_STEM_COLOUR:
									visualParametrs.setStemColour(visualElement.item(v).getTextContent());
									break;
								case XMLflowerConstant.VP_LEAF_COLOUR:
									visualParametrs.setLeafColour(visualElement.item(v).getTextContent());
									break;
								case XMLflowerConstant.VP_AVE_LEN_FLOWER:
									visualParametrs.setAveLenFlower(visualElement.item(v).getTextContent());
									break;
							}
						}
						flower.setVisualParameters(visualParametrs);
						break;
					case XMLflowerConstant.GROWING_TIPS:
						NodeList growElements = elementList.item(i).getChildNodes();
						GrowingTips growElement = new GrowingTips();
						for (int g = 0; g < growElements.getLength(); g++) {
							if (growElements.item(g).getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}
							switch (growElements.item(g).getNodeName()) {
								case XMLflowerConstant.GT_TEMPRETURE:
									growElement.setTempreture(growElements.item(g).getTextContent());
									break;
								case XMLflowerConstant.GT_LIGHTING:
									growElement.setLighting(growElements.item(g).getAttributes().getNamedItem("lightRequiring")
											.getTextContent());
									break;
								case XMLflowerConstant.GT_WATERING:
									growElement.setWatering(growElements.item(g).getTextContent());
									break;
							}
						}

						flower.setGrowingTips(growElement);
						break;
					case XMLflowerConstant.MULTIPLYING:
						flower.setMultiplying(elementList.item(i).getTextContent());
						break;

				}

			}
			flowers.add(flower);
		}


	}

	public Document createDocument() throws ParserConfigurationException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
		Document document = documentBuilder.newDocument();

		Element root = document.createElement(XMLflowerConstant.FLOWERS);

		root.setAttribute("xmlns", "http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

		document.appendChild(root);

		Element element;
		for (Flower flower : flowers) {
			Element flowerElement = document.createElement(XMLflowerConstant.FLOWER);

			List<Object> listFlower = Util.flowerToList(flower);
			List<Object> listedVisual = listFlower.subList(3, 6);
			List<Object> listedGrowing = listFlower.subList(6, 9);

			for (int i = 0; i < 3; i++) {
				element = document.createElement(XMLflowerConstant.FLOWER_ARRAY[i]);
				element.setTextContent(listFlower.get(i).toString());
				flowerElement.appendChild(element);
			}

			flowerElement.appendChild(getVisualParametrs(document, listedVisual));
			flowerElement.appendChild(getGrowingTips(document, listedGrowing));

			element = document.createElement(XMLflowerConstant.FLOWER_ARRAY[5]);
			element.setTextContent(listFlower.get(9).toString());
			flowerElement.appendChild(element);
			root.appendChild(flowerElement);
		}
		return document;
	}

	private Element getVisualParametrs(Document document, List<Object> listedVisual) {
		Element visualParam = document.createElement(XMLflowerConstant.VISUAL_PARAMETRS);
		Element childElement;
		for (int i = 0; i < XMLflowerConstant.VISUAL_PARAMETRS_ARRAY.length; i++) {
			childElement = document.createElement(XMLflowerConstant.VISUAL_PARAMETRS_ARRAY[i]);
			childElement.setTextContent(listedVisual.get(i).toString());
			if (i == 2)
				childElement.setAttribute("measure", "cm");
			visualParam.appendChild(childElement);
		}
		return visualParam;
	}

	private Element getGrowingTips(Document document, List<Object> listedGrowing) {
		Element growingTips = document.createElement(XMLflowerConstant.GROWING_TIPS);
		Element childElement;
		for (int i = 0; i < XMLflowerConstant.GROWING_TIPS_ARRAY.length; i++) {

			childElement = document.createElement(XMLflowerConstant.GROWING_TIPS_ARRAY[i]);
			switch (i) {
				case 0:
					childElement.setAttribute("measure", "celcius");
					childElement.setTextContent(listedGrowing.get(i).toString());
					break;

				case 1:
					childElement.setAttribute("lightRequiring", listedGrowing.get(i).toString());
					break;
				case 2:
					childElement.setAttribute("measure", "mlPerWeek");
					childElement.setTextContent(listedGrowing.get(i).toString());
					break;
			}
			growingTips.appendChild(childElement);
		}
		return growingTips;
	}

	public void saveToXML(String url) throws TransformerException, ParserConfigurationException {
		StreamResult result = new StreamResult(new File(url));
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		t.transform(new DOMSource(createDocument()), result);
	}

}
