package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import java.util.Collections;

import com.epam.rd.java.basic.task8.comparators.FlowersComparatorByName;
import com.epam.rd.java.basic.task8.comparators.FlowersComparatorByOrigin;
import com.epam.rd.java.basic.task8.comparators.FlowersComparatorBySoil;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);

		domController.parseXML(true);

		// sort (case 1)

		Collections.sort(domController.getFlowers(), new FlowersComparatorByName());
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.saveToXML(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		saxController.parseXML(true);
		
		// sort  (case 2)

		Collections.sort(saxController.getFlowers(), new FlowersComparatorBySoil());
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.saveToXML(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);

		staxController.parseXML(true);
		
		// sort  (case 3)

		Collections.sort(staxController.getFlowers(), new FlowersComparatorByOrigin());
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.saveToXML(outputXmlFile);
	}

}
