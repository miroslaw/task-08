package com.epam.rd.java.basic.task8;

import java.util.Arrays;
import java.util.List;

public class Flower {
	private static final List<String> SOIL_ENUM = Arrays.asList("подзолистая", "грунтовая",  "дерново-подзолистая");
	private static final List<String> MULTIPLYING_ENUM = Arrays.asList("листья", "черенки", "семена" );
	private String name;
	private String soil;
	private String origin;
	private VisualParametrs visualParameters;
	private GrowingTips growingTips;
	private String multiplying;


	
	public Flower(String name, String soil, String origin, String multiplying) {
		this.name = name;
		this.soil = validateSoil(soil);
		this.origin = origin;
		this.multiplying = validateMultiplying(multiplying);
	}
	
	public Flower() {
		
	}
	
	
	public String validateSoil(String s) {
		if(SOIL_ENUM.contains(s)) {
			return s;
		}
		return "";
	}
	
	
	public String validateMultiplying(String s) {
		if(MULTIPLYING_ENUM.contains(s)) {
			return s;
		}
		return "";
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSoil() {
		return soil;
	}
	public void setSoil(String soil) {
		this.soil = soil;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public VisualParametrs getVisualParameters() {
		return visualParameters;
	}
	public void setVisualParameters(VisualParametrs visualParameters) {
		this.visualParameters = visualParameters;
	}
	public GrowingTips getGrowingTips() {
		return growingTips;
	}
	public void setGrowingTips(GrowingTips growingTips) {
		this.growingTips = growingTips;
	}
	public String getMultiplying() {
		return multiplying;
	}
	public void setMultiplying(String multiplying) {
		this.multiplying = multiplying;
	}

	
	@Override
	public String toString() {
		return "\n-------------------------\n" + "Flower name: " + name + "\nSoil: " + soil
				+ "\nOrigin: " + origin + "\n" + visualParameters + "\n" + growingTips + "\nmultiplying: " + multiplying;
	}

	
}
