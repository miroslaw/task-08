package com.epam.rd.java.basic.task8;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Path;
import java.security.cert.CertPath;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.epam.rd.java.basic.task8.constant.XMLflowerConstant;

public class Util {
	
	
	public static List<Object> flowerToList(Flower flower){
		List<Object> resultList = new ArrayList<>();
		
		resultList.add(flower.getName());
		resultList.add(flower.getSoil());
		resultList.add(flower.getOrigin());
		
		VisualParametrs visualParametrs = flower.getVisualParameters();
		resultList.add(visualParametrs.getStemColour());
		resultList.add(visualParametrs.getLeafColour());
		resultList.add(visualParametrs.getAveLenFlower());
		
		GrowingTips growingTips = flower.getGrowingTips();
		resultList.add(growingTips.getTempreture());
		resultList.add(growingTips.getLighting());
		resultList.add(growingTips.getWatering());
		
		resultList.add(flower.getMultiplying());
		
		return resultList;
	}

	
	public static void writeSaxStax(List<Flower> flowers, String path) throws IOException, XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance()	;
		XMLStreamWriter streamWriter = null;
		
	
		try(FileWriter fileWriter = new FileWriter(path)) {
			streamWriter = outputFactory.createXMLStreamWriter(fileWriter);
	
			streamWriter.writeStartDocument("UTF-8", "1.0");
			
			streamWriter.writeStartElement(XMLflowerConstant.FLOWERS);
			streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
			streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
			
			for(Flower flower : flowers) {
				List<Object> listedFlower = Util.flowerToList(flower);
				List<Object> listedVisual = listedFlower.subList(3, 6);
				List<Object> listedGrowing = listedFlower.subList(6, 9);
				streamWriter.writeStartElement(XMLflowerConstant.FLOWER);
				for(int i=0; i<3; i++) {
					streamWriter.writeStartElement(XMLflowerConstant.FLOWER_ARRAY[i]);
					streamWriter.writeCharacters(listedFlower.get(i).toString());
					streamWriter.writeEndElement();
				}
				writeVisual(streamWriter, listedVisual);
				writeGrowing(streamWriter, listedGrowing);
				
				streamWriter.writeStartElement(XMLflowerConstant.FLOWER_ARRAY[5]);
				streamWriter.writeCharacters(listedFlower.get(9).toString());
				streamWriter.writeEndElement();
				
				streamWriter.writeEndElement();
			}
			streamWriter.writeEndElement();
			
			streamWriter.writeEndDocument();
			
			
			streamWriter.flush();
			streamWriter.close();
		}

	}
	
	private static void writeVisual(XMLStreamWriter writer, List<Object> list) throws XMLStreamException {
		writer.writeStartElement(XMLflowerConstant.VISUAL_PARAMETRS);
			
		for(int i=0; i<list.size(); i++) {
			writer.writeStartElement(XMLflowerConstant.VISUAL_PARAMETRS_ARRAY[i]);
			
			if(i==2) writer.writeAttribute("measure","cm"); ;
 			writer.writeCharacters(list.get(i).toString());
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}
	
	private static void writeGrowing(XMLStreamWriter writer, List<Object> list) throws XMLStreamException {
		writer.writeStartElement(XMLflowerConstant.GROWING_TIPS);
		
		for(int i=0; i<list.size(); i++) {
			
			if(i==1) {
				writer.writeEmptyElement(XMLflowerConstant.GT_LIGHTING);
				writer.writeAttribute("lightRequiring","yes");
			} else {
				writer.writeStartElement(XMLflowerConstant.GROWING_TIPS_ARRAY[i]);
				if(i==0) writer.writeAttribute("measure","celcius");
				if(i==2) writer.writeAttribute("measure","mlPerWeek");
				writer.writeCharacters(list.get(i).toString());
			
				writer.writeEndElement();
			}
				
				
			
			
		}
		writer.writeEndElement();
		
	}
	
	
	
}
